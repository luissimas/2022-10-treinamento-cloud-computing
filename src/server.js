import express from "express";
import userRouter from "./controllers/user-controller.js";

const app = express();

app.use(userRouter);

app.get("/", (req, res) => res.json({ hello: "world", opa: "show de bola" }));

export default app;
